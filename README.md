# How To
## Prerequisites
As this uses docker containers, you only need
- docker-ce ([install](https://docs.docker.com/install/linux/docker-ce/ubuntu/))
  - (optional) make docker usable as non root `sudo gpasswd -a $USER docker`. Log out and in again to refresh the groups
- docker-compose ([install](https://docs.docker.com/compose/install/))
- sm64 decomp
- original ROM (for asset extraction) (good luck ¯\\\_(ಥ‿ಥ)\_/¯)

## Preparation
0. `cd` into the this cloned project
1. Run `docker-compose build` to create an image that handles building the ROM
2. Put `your-project` as a folder under `projects` (so you can have multiple projects located there)  
   (`git clone https://github.com/n64decomp/sm64 ./projects/your-project`)
3. Put your original ROM in `projects/your-project/baserom.us.z64` (adjust the version (us, eu, jp))
   for asset extraction
4. Adjust the `docker-compose.yml`
   - Add a service with a descriptive name (you can orientate to the service `original`)
   - Use the `sm64-builder` image with the tag `builder` (`sm64-builder:builder`)
   - Give some environment variables
     - `SILENT_BUILD`: `true` for muting the make process, anything else for not muting it
     - `BUILD_NAME`: specifies the name of the build in the `build` directory
   - Specify some volumes (**IMPORTANT**)
     - `./projects/name-of-your-project:/app`: This gives the container access to the project
     - `./build:/build`: To make the build accessible outside of the container

## Building
Run the `build.sh` and give it the name of the docker-compose service.
You can also give it some arguments for `make`  
Examples:
- `bash ./build.sh original`
- `bash ./build.sh original VERSION=eu`
- `bash ./build.sh your-project -j4`

## Troubleshooting
1. Q: I get a `permission denied` while building

   A: make sure that `projects/your-project/tools` has execution permissions (run `chmod -R 755 projects/your-project/tools` for a quick'n'dirty fix)

2. Q: Where do I get the sm64 decomp from?

   A: `git clone https://github.com/n64decomp/sm64 ./projects/sm64`

3. Q: I get an error `sha1sum: WARNING: 1 computed checksum did NOT match`

   A: Run `build.sh` with `COMPARE=0` or edit the projects Makefile  
      Find these lines and delete them or put a comment (`#`) at the beginning
      ```makefile
      ifeq ($(COMPARE),1)
          @$(SHA1SUM) -c $(TARGET).sha1
      endif
      ```

4. Q:

   A:

