#!/usr/bin/env bash

name=${1}

if [[ "$name" == "" ]];
then
    echo "pls giev name!"
    echo "must be one of"
    echo
    cat docker-compose.yml | grep "^  [a-zA-Z_-]*:$" | tr -d ":"
    echo
    exit
fi

# Unsetting first parameter
params=( $* )
unset params['0']
set -- "${params[@]}"

docker-compose run --rm ${name} "$@"

home=$(realpath ~)

if ls build/${name}/*/sm64.*.z64 > /dev/null 2>&1 && [[ -d "${home}/shared/N64/" ]];
then
    cp build/${name}/*/sm64.*.z64 ~/shared/N64/${name}.z64
    echo
    echo "Copied ROM to ~/shared/N64/${name}.z64"
fi
