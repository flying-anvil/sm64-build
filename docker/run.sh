#!/usr/bin/env bash

set -e
set -o pipefail

silectBuild=${SILENT_BUILD}
buildName=${BUILD_NAME}

if [[ "$buildName" == "" ]]; then
    buildName="default"
fi

buildPath="/build/${buildName}"

if [[ -d "${buildPath}" ]]; then
    echo "found old build, removing"
    rm -rf ${buildPath}
    echo "done"
    echo
fi

mkdir -p ${buildPath}

echo "building ${buildName}"

cmd="make $@"

if [[ "${silectBuild}" == "true" ]]; then
    cmd="make $@ > /dev/null 2>/dev/null"
fi

echo
echo 'command is:' ${cmd}
echo

if eval ${cmd}; then
    echo "done"
    echo
    echo "moving build context to /build"

    mv ./build/* ${buildPath}
    rm -rf ./build
    chmod -R 777 /build
    chown -R 1000:1000 ${buildPath}

    echo "done"
    echo

    sha1=$(sha1sum ${buildPath}/*/sm64.*.z64 | awk '{ print $1 }')
    sha256=$(sha256sum ${buildPath}/*/sm64.*.z64 | awk '{ print $1 }')
    sha512=$(sha512sum ${buildPath}/*/sm64.*.z64 | awk '{ print $1 }')
    md5=$(md5sum ${buildPath}/*/sm64.*.z64 | awk '{ print $1 }')

    echo "sha1:   ${sha1}"
    echo "sha256: ${sha256}"
    echo "sha512: ${sha512}"
    echo "md5:    ${md5}"
else
    echo "there was an error, cleaning up"
    rm -rf ${buildPath}
    echo "done"
fi
