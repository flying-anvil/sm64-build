#!/usr/bin/env bash

images=$(docker images -f "dangling=true" -q)

if [[ "${images}" == "" ]]
then
    echo "no images to delete"
    exit
fi

docker rmi ${images}
